INTRODUCTION
------------

This module allows site developers to use https://www.abstractapi.com/ip-geolocation-api API.

Usage:

Use Dependency Injection for getting Abstract IP geolocation service or use
\Drupal::service('abstract_ip_geolocation').

use Drupal\abstract_ip_geolocation\AbstractIpGeolocation;
$abstractIpGeolocation = \Drupal::service('abstract_ip_geolocation');
$abstractIpGeolocation
  ->setIp($ip)
  ->setOptions($options);
$data = $abstractIpGeolocation->getData();

REQUIREMENTS
------------

No requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   Visit: https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* Configure at: [Your Site]/admin/config/system/abstract_ip_geolocation.
You need set your Abstract IP geolocation API Key. You can get the key from the
https://www.abstractapi.com/ip-geolocation-api.

* Test at: [Your Site]/admin/config/system/abstract_ip_geolocation/test


MAINTAINERS
-----------

Current maintainers:
 * Sergey Loginov (goodboy) - https://drupal.org/user/222910
