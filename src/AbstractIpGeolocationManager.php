<?php

namespace Drupal\abstract_ip_geolocation;

/**
 * @file
 * Contains \Drupal\abstract_ip_geolocation\AbstractIpGeolocationManagerManager.
 */

use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client as HttpClient;

/**
 * Class AbstractIpGeolocationManager.
 *
 * Provides Abstract IP geolocation service.
 *
 * @ingroup abstract_ip_geolocation
 */
class AbstractIpGeolocationManager extends AbstractIpGeolocation {

  /**
   * AbstractIpGeolocationManager constructor.
   */
  public function __construct(HttpClient $http_client) {
    parent::__construct();
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

}
