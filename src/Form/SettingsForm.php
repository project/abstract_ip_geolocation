<?php

namespace Drupal\abstract_ip_geolocation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Configure abstract_ip_geolocation settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abstract_ip_geolocation_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'abstract_ip_geolocation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Abstract IP Geolocation API Key'),
      '#default_value' => $this->config('abstract_ip_geolocation.settings')->get('api_key'),
      '#required' => TRUE,
      '#description' => $this->t("Get API Key by register at
        <a href='@url' rel='nofollow' target='_new'>Abstract IP geolocation</a>.",
        ['@url' => 'https://www.abstractapi.com/ip-geolocation-api']
      ),
    ];

    $form['use_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use cache'),
      '#default_value' => !empty($this->config('abstract_ip_geolocation.settings')->get('use_cache')),
      '#description' => $this->t('Use cache data for IP instead Abstract IP geolocation API request.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('abstract_ip_geolocation.settings');

    if ($config->get('use_cache')) {
      // Invalidate abstract_ip_geolocation cache.
      Cache::invalidateTags(['abstract_ip_geolocation']);
      $this->messenger()->addMessage($this->t('Abstract IP geolocation cash was cleared.'));
    }

    $config->set('api_key', $form_state->getValue('api_key'))
      ->set('use_cache', $form_state->getValue('use_cache'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
