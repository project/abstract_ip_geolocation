<?php

namespace Drupal\abstract_ip_geolocation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\abstract_ip_geolocation\AbstractIpGeolocationManager;

/**
 * Configure Abstract IP geolocation settings for this site.
 */
class TestForm extends FormBase {

  /**
   * Ipstack manager variable.
   *
   * @var \Drupal\abstract_ip_geolocation\AbstractIpGeolocationManager
   */
  protected $abstractIpGeolocation;

  /**
   * Construct the AbstractIpGeolocationManager.
   *
   * @param \Drupal\abstract_ip_geolocation\AbstractIpGeolocationManager $abstractIpGeolocation_manager
   *   Store RequestStack manager.
   */
  public function __construct(AbstractIpGeolocationManager $abstractIpGeolocation_manager) {
    $this->abstractIpGeolocation = $abstractIpGeolocation_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('abstract_ip_geolocation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abstract_ip_geolocation_admin_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('abstract_ip_geolocation.settings');
    $user_ip = $this->getRequest()->getClientIp();
    $ip = $config->get('ip') ?: $user_ip;

    $form['ip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP address'),
      '#default_value' => $ip,
      '#required' => TRUE,
      '#description' => $this->t("IP address for test retrieving
        <a href=':url' rel='nofollow' target='_new'>Abstract IP geolocation</a> data.
        Your current IP: @user_ip .",
        [':url' => 'https://www.abstractapi.com/ip-geolocation-api', '@user_ip' => $user_ip]
      ),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('abstract_ip_geolocation.settings');

    if ($config->get('use_cache')) {
      // Invalidate abstract_ip_geolocation cache.
      Cache::invalidateTags(['abstract_ip_geolocation']);
      $this->messenger()->addMessage($this->t('Abstract IP geolocation cash was cleared.'));
    }

    $ip = $form_state->getValue('ip');
    $config->set('ip', $ip);
    $config->save();

    // Make Abstract IP geolocation request and show result.
    $this->abstractIpGeolocation
      ->setIp($ip)
      ->setOptions()
      ->showResult();
  }

}
