<?php

namespace Drupal\abstract_ip_geolocation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\abstract_ip_geolocation\AbstractIpGeolocationManager;

/**
 * Controller for page /admin/config/system/abstract_ip_geolocation/test/page.
 */
class TestController extends ControllerBase {

  /**
   * AbstractIpGeolocation manager variable.
   *
   * @var \Drupal\abstract_ip_geolocation\AbstractIpGeolocationManager
   */
  protected $abstractIpGeolocation;

  /**
   * Request stack variable.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new AbstractIpGeolocationSettingsForm object.
   *
   * @param \Drupal\abstract_ip_geolocation\AbstractIpGeolocationManager $abstractIpGeolocation_manager
   *   Store RequestStack manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Store RequestStack manager.
   */
  public function __construct(AbstractIpGeolocationManager $abstractIpGeolocation_manager, RequestStack $request_stack) {
    $this->abstractIpGeolocation = $abstractIpGeolocation_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('abstract_ip_geolocation'),
      $container->get('request_stack')
    );
  }

  /**
   * Show test page.
   */
  public function page() {
    $config = $this->config('abstract_ip_geolocation.settings');
    $ip = $this->requestStack->getCurrentRequest()->query->get('ip');
    if (!empty($ip)) {
      $output = $this->t('Abstract IP geolocation test for IP %ip', ['%ip' => $ip]);

      // Make AbstractIpGeolocation request and show result.
      $this->abstractIpGeolocation
        ->setIp($ip)
        ->setOptions()
        ->showResult();
    }
    else {
      $ip = $config->get('ip') ?: $this->requestStack->getCurrentRequest()->getClientIp();
      $url = Url::fromRoute('abstract_ip_geolocation.testpage', ['ip' => $ip])->setAbsolute();
      $link = Link::fromTextAndUrl($url->toString(), $url)->toString();
      $output = $this->t('Need IP parameter like ?ip=@ip. For example: @link', [
        '@ip' => $ip,
        '@link' => $link,
      ]);
    }

    return ['#markup' => $output];
  }

}
