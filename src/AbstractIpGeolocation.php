<?php

namespace Drupal\abstract_ip_geolocation;

/**
 * @file
 * Contains \Drupal\abstract_ip_geolocation\AbstractIpGeolocation.
 */

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;
use Drupal\Core\Cache\CacheBackendInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractIpGeolocation.
 *
 * Provides Abstract IP geolocation.com API.
 *
 * @ingroup abstract_ip_geolocation
 */
class AbstractIpGeolocation extends ControllerBase {

  const SERVICE_URL = 'https://ipgeolocation.abstractapi.com/v1/';

  /**
   * IP address.
   *
   * @var string
   */
  protected $ip;

  /**
   * AbstractIpGeolocation options.
   *
   * @var array
   */
  protected $options;

  /**
   * Is abstract_ip_geolocation data from cache?
   *
   * @var bool
   */
  protected $cacheData;

  /**
   * GuzzleHttp\Client definition.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * AbstractIpGeolocation constructor.
   *
   * @param string $ip
   *   IP address.
   * @param array $options
   *   AbstractIpGeolocation options.
   */
  public function __construct($ip = '', array $options = []) {
    $this->setIp($ip);
    $this->setOptions($options);
    $this->cacheData = FALSE;
    $this->httpClient = NULL;
  }

  /**
   * Set IP address.
   *
   * @param string $ip
   *   IP address.
   */
  public function setIp($ip) {
    $this->ip = trim($ip);
    return $this;
  }

  /**
   * Set abstract_ip_geolocation options. (config value if empty).
   *
   * @param string $options
   *   Abstract IP g eolocation options.
   */
  public function setOptions($options = []) {
    if (empty($options['api_key'])) {
      $api_key = $this->config('abstract_ip_geolocation.settings')->get('api_key');
      $options['api_key'] = $api_key;
    }
    $this->options = $options;

    return $this;
  }

  /**
   * Get abstract_ip_geolocation URL.
   *
   * @return string
   *   Abstract IP geolocation URL for data retrieving.
   */
  public function getUrl() {
    $options = array_merge(['ip_address' => $this->ip], $this->options);
    $url_options = ['absolute' => TRUE, 'query' => $options];
    $url = Url::fromUri(self::SERVICE_URL, $url_options);
    return $url->toString();
  }

  /**
   * Get abstract_ip_geolocation data.
   *
   * @return array
   *   AbstractIpGeolocation data responce.
   */
  public function getData() {
    $use_cache = $this->config('abstract_ip_geolocation.settings')->get('use_cache');
    $this->cacheData = FALSE;

    // Get data from cache.
    $cid = 'abstract_ip_geolocation:ip_' . $this->ip;
    if ($use_cache) {
      $cache = $this->cache()->get($cid);
      if (!empty($cache)) {
        $this->cacheData = TRUE;
        return ['data' => $cache->data];
      }
    }

    // Get data from abstract_ip_geolocation site.
    // API Key is required.
    if (empty($this->options['api_key'])) {
      return ['error' => $this->t('Empty Abstract IP geolocation API key.')];
    }

    // Get data from api.abstract_ip_geolocation.com .
    $client = $this->httpClient;
    if (empty($client)) {
      return ['error' => $this->t('Empty http client.')];
    }

    try {
      $responce = $client->get($this->getUrl());
    }
    catch (\Exception $exception) {
      return [
        'error' => $this->t('Error retrieving data: @message', [
          '@message' => $exception->getMessage(),
        ]),
      ];
    }

    $status = $responce->getStatusCode();
    if ($status != Response::HTTP_OK) {
      return ['error' => $this->t('Got responce status @status', ['@status' => $status])];
    }

    $data = (string) $responce->getBody();
    if ($use_cache) {
      // Set abstract_ip_geolocation cache.
      $this->cache()->set($cid, $data, CacheBackendInterface::CACHE_PERMANENT, ['abstract_ip_geolocation']);
    }

    return ['data' => $data];
  }

  /**
   * Show result.
   */
  public function showResult() {
    $url = $this->getUrl();
    $messenger = $this->messenger();
    $msg = $this->t("Request: <a href=':url' target='_new'>:url</a>", [':url' => $url]);
    $messenger->addMessage($msg);

    $data = $this->getData();

    if (!empty($data['error'])) {
      $msg = $data['error'];
      $messenger->addMessage($msg, 'error');
    }

    if (!empty($data['data'])) {
      $data = $data['data'];
      if ($this->cacheData) {
        $messenger->addMessage($this->t('From cache'));
      }

      // Decode JSON object.
      $data = json_decode($data);

      $status = 'status';
      if (!empty($data->error)) {
        $data = $data->error;
        $status = 'error';
      }

      $msg = $this->t('Responce:');
      if (is_array($data) || is_object($data)) {
        $msg .= ' <pre>' . print_r($data, 1) . '</pre>';
        $msg = Markup::create($msg);
      }
      else {
        $msg .= $data;
      }
      $messenger->addMessage($msg, $status);
    }

    $messenger->all();
  }

}
